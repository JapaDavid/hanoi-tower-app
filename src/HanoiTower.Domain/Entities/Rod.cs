using System.Collections.Generic;
using System.Linq;

namespace HanoiTower.Domain.Entities
{
    public class Rod
    {
        public int Position { get; set; }
        public IList<Disk> Disks { get; set; }

        public Rod()
        {
            Disks = new List<Disk>();
        }

        public void AddDisks(int amount)
        {
            for (int i = 1; i <= amount; i++)
                Disks.Add(new Disk { Size = i });
        }

        public Disk FindDisk(int size)
        {
            var disk = Disks.FirstOrDefault(x => x.Size == size);

            return disk;
        }

        public void RemoveDisk(Disk disk)
        {
            Disks.Remove(disk);
        }

        public void AddDisk(Disk disk)
        {
            Disks.Add(disk);
        }
    }
}
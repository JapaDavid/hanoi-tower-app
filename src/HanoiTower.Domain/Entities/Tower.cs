namespace HanoiTower.Domain.Entities
{
    public class Tower
    {
        public Rod FirstRod { get; set; }
        public Rod SecondRod { get; set; }
        public Rod ThirdRod { get; set; }

        private int _moveCount { get; set; }

        public Tower(int disksAmount)
        {
            FirstRod = new Rod();
            SecondRod = new Rod();
            ThirdRod = new Rod();

            FirstRod.AddDisks(disksAmount);
        }

        public int SolvePuzzle()
        {
            var totalDisks = FirstRod.Disks.Count;

            SolveSteps(totalDisks, FirstRod, ThirdRod, SecondRod);

            return _moveCount;
        }

        private void SolveSteps(int diskSize, Rod source, Rod dest, Rod aux)
        {
            _moveCount++;

            if (diskSize == 1)
            {
                MoveDisk(diskSize, source, dest);
                return;
            }

            SolveSteps(diskSize - 1, source, aux, dest);
            MoveDisk(diskSize, source, dest);
            SolveSteps(diskSize - 1, aux, dest, source);
        }

        public void MoveDisk(int diskSize, Rod originRod, Rod destinationRod)
        {
            var disk = originRod.FindDisk(diskSize);
            originRod.RemoveDisk(disk);
            destinationRod.AddDisk(disk);
        }
    }
}